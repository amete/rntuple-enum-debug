/**
 * Author: Alaettin Serhan Mete <amete@anl.gov>
 */

#include <vector>

// Dummy Class to define the Enum
class MyEnumClass {
  public:
    enum MyEnum { ZERO=0, ONE, UNKNOWN };
};

// This is the class that gets persistified
class MyPersClass {
  public:
    MyPersClass() : m_float(-1.f), m_enum(MyEnumClass::UNKNOWN) { }
    MyPersClass(const float fval, const MyEnumClass::MyEnum eval) : m_float(fval), m_enum(eval) { }

    float get_float() const { return m_float; }
    MyEnumClass::MyEnum get_enum() const { return m_enum; }

  private:
    float m_float;
    MyEnumClass::MyEnum m_enum;
};

// Instantiate all necessary types for the dictionary
namespace {
  struct GCCXML_DUMMY_INSTANTIATION {
    std::vector<MyPersClass> dummy;
  };
}
