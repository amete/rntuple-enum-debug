/**
 * Author: Alaettin Serhan Mete <amete@anl.gov>
 */

#define VERSION 0 // 0: RNTuple, 1: TTree

#include <ROOT/RNTuple.hxx>
#include <ROOT/RNTupleModel.hxx>
#include <TFile.h>
#include <TTree.h>

#include "MyPersClass.h"

#include <iostream>
#include <string>
#include <vector>

using RNTupleModel = ROOT::Experimental::RNTupleModel;
using RNTupleReader = ROOT::Experimental::RNTupleReader;
using RNTupleWriter = ROOT::Experimental::RNTupleWriter;

int main(int argc, char *argv[]) {
  // Make sure we have enough arguments
  if ((argc) != 3)
    exit(1);

  // Set local variables
  std::string ntupleName = std::string(argv[1]);
  std::string outputName = std::string(argv[2]);

  // Write Here
  {
    if(VERSION == 0) { // RNTuple version
      auto model = RNTupleModel::Create();
      auto field = model->MakeField<std::vector<MyPersClass>>("foo");
      auto ntuple = RNTupleWriter::Recreate(std::move(model), ntupleName, outputName);
      for (unsigned i = 0; i < 10; ++i) {
        field->clear();
        for (unsigned j = 0; j < 10; ++j) {
          field->emplace_back(i*j, j<5 ? MyEnumClass::ZERO : MyEnumClass::ONE);
        }
        ntuple->Fill();
      }
    } else if(VERSION == 1) { // TTree version
      TFile* f = TFile::Open(outputName.c_str(), "RECREATE");
      TTree* t = new TTree(ntupleName.c_str(), "");
      std::vector<MyPersClass> result;
      t->Branch("foo", &result);
      for (unsigned i = 0; i < 10; ++i) {
        result.clear();
        for (unsigned j = 0; j < 10; ++j) {
          result.emplace_back(i*j, j<5 ? MyEnumClass::ZERO : MyEnumClass::ONE);
        }
        t->Fill();
      }
      f->Write();
      f->Close();
    }
  }

  // Read Here
  {
    if(VERSION == 0) { // RNTuple version
      auto ntuple = RNTupleReader::Open(ntupleName, outputName);
      ntuple->Show(9);
    } else if (VERSION == 1) { // TTree version
      TFile* f = TFile::Open(outputName.c_str(), "OPEN");
      TTree* t = f->Get<TTree>(ntupleName.c_str());
      t->Show(9);
    }
  }

  return 0;
}
