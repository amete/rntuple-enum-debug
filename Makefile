### VARIABLES ###################################################################

# Check to see if ROOT is configured
ifeq ($(shell root-config --cflags),)
  $(error Cannot find root-config. Please source thisroot.sh)
endif

# Set ROOT flags
CXXFLAGS_ROOT = $(shell root-config --cflags)
LDFLAGS_ROOT = $(shell root-config --libs) -lROOTNTuple -lROOTNTupleUtil

# Set Custom flags
CXXFLAGS_CUSTOM = -Wall -pthread -Wall -g -O2  
LDFLAGS_CUSTOM =

# Put all flags together
CXXFLAGS = $(CXXFLAGS_CUSTOM) $(CXXFLAGS_ROOT)
LDFLAGS = $(LDFLAGS_CUSTOM) $(LDFLAGS_ROOT)  

### BINARIES ###################################################################

WriteReadEnum: src/WriteReadEnum.cxx
	g++ $(CXXFLAGS) -o bin/$@ $< -Llib/. -lMyPersClassDict $(LDFLAGS)

all:
	+$(MAKE) libMyPersClassDict.so -C src
	+$(MAKE) WriteReadEnum

### CLEAN ######################################################################

clean:
	rm -f bin/* src/*Dict* lib/*Dict* 
